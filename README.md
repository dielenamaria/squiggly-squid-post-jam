## Credits

Music (by Kevin MacLeod):

- "Easy Lemon"
- "Arcadia"
- "Water Lily"

Sound effects:

- https://freesound.org/people/Joao_Janz/sounds/478712/
- https://freesound.org/people/InspectorJ/sounds/398721/
- https://freesound.org/people/Benboncan/sounds/112662/
- https://freesound.org/people/klankbeeld/sounds/267245/
