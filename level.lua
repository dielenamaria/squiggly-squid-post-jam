-- a class for levels 
-- and some functions for managing them

local Level = class("Level")

function Level:initialize(name, layers, intro, outro, music, gradientDepth)
    self.name = name
    self.polygons = polygons
    self.objectSpec = objects

    self.started = false
    self.won = false

    self.debug = false

    -- nil is acceptable as a default value here!
    self.intro = intro
    -- start immediately if the level has no intro
    if self.intro == nil then
        self:start()
    end

    -- default outro text
    if outro ~= nil then
        self.outro = outro
    else
        self.outro = "Congratulations, you won! Click to continue."
    end

    self.music = music or music.arcadia
    self.musicPosition = 0

    self.layers = {}
    for i = 1,9 do
        table.insert(self.layers, Layer:new(i))
        if (#layers == 9) then
            self.layers[i].polygons = layers[i].polygons
            self.layers[i].objects = layers[i].objects
        end
    end

    self.gameLayer = self.layers[5]

    if (#layers == 1) then
        self.gameLayer.polygons = layers[1].polygons
        self.gameLayer.objects = layers[1].objects
    end

    self.gradient = Gradient:new(gradientDepth or 15000)
    for i = 1,9 do
        self.layers[i]:preparePolygons(self.gradient)
    end
    love.graphics.setBackgroundColor( 0.0, 0.0, 0.2)

    for i,object in ipairs(self.gameLayer.objects) do
        if object.class.name == "Squid" then
            self.squid = object
        end
    end
    if not self.squid then
        print("no squid found, creating emergency squid")
        self.squid = Squid:new(vector.new(1500,400))
        self.squid:setPosition(vector(CANVAS_WIDTH, CANVAS_HEIGHT/2))
        table.insert(self.gameLayer.objects, self.squid)
    end
    

    self.state = "dead"
end

function Level:setState(targetState)
    if self.state == targetState then
        return
    end

    if self.state == "dead" then
        if targetState == "editor" then
            for i,layer in ipairs(self.layers) do
                layer:preparePolygons(self.gradient)
            end
        end
        if targetState == "game" then
            for i,layer in ipairs(self.layers) do
                layer:preparePolygons(self.gradient)
                layer:prepareWorld()
            end
            if not muted then
                self.music:play()
            end
            self.wantsCameraReset = true

            -- each layer has its own world, but we alias the game layer's world as the level's world
            self.world = self.gameLayer.world
        end
    end
    if self.state == "editor" then
        if targetState == "dead" then
            for i,layer in ipairs(self.layers) do
                layer:cleanupPolygons()
                layer:cleanupWorld()
            end
        end
        if targetState == "game" then
            for i,layer in ipairs(self.layers) do
                layer:prepareWorld()
            end
            if not muted then
                self.music:play()
            end
            self.wantsCameraReset = true

            -- each layer has its own world, but we alias the game layer's world as the level's world
            self.world = self.gameLayer.world
        end
    end
    if self.state == "game" then
        if targetState == "dead" then
            for i,layer in ipairs(self.layers) do
                layer:cleanupPolygons()
                layer:cleanupWorld()
            end
            self:cleanup()
            self.music:stop()
            self.musicPosition = 0
        end
        if targetState == "editor" then
            for i,layer in ipairs(self.layers) do
                layer:cleanupWorld()
            end
            self.music:stop()
            self.musicPosition = 0
        end
    end

    self.state = targetState
end

function Level:update(dt)
    assert(self.state == "game")
    for i,layer in ipairs(self.layers) do
        layer:update(dt)
    end

    for i, object in ipairs(self.gameLayer.objects) do
        if object.collected then
            -- Object is collected? Let's destroy it, har har!
            removeValue(self.gameLayer.objects, object)
            object.body:destroy()
        end
    end

    if input:isPressed("debug") then
        self.debug = not self.debug
    end
end

function Level:draw(camera, onlyLayer)
    assert(self.state ~= "dead")
    if self.won == true then
        love.graphics.setColor(1,1,1,1)
        love.graphics.draw(images.back3)
        love.graphics.setColor(textColor)
        love.graphics.printf(self.outro, 150, (CANVAS_HEIGHT-300)/2, CANVAS_WIDTH - 300, "center")
        love.graphics.setColor(1,1,1,1)
    elseif (self.started == false and self.intro ~= nil) then
        love.graphics.setColor(1,1,1,1)
        love.graphics.draw(images.back3)
        love.graphics.setColor(textColor)
        love.graphics.printf(self.intro, 150, 150, CANVAS_WIDTH - 300, "center")
        love.graphics.setColor(1,1,1,1)
    else
        self.gradient:draw(camera)
        -- Draw layers.
        for i=9,1,-1 do
            if not onlyLayer or i == onlyLayer then
                self.layers[i]:draw(camera, self.debug and i==5, self.world)
            end
        end
    end
end

function Level:start()
    self.started = true
end

function Level:cleanup()
    self.won = false
    if self.intro ~= nil then
        self.started = false
    end
end

function Level:isWon()
    return self.won
end

function beginContact(a, b, contact)
    local aData = a:getBody():getUserData()
    local bData = b:getBody():getUserData()

    if not (aData and bData) then
        return
    end

    local aName = aData.class.name
    local bName = bData.class.name

    -- Swap squid always to go first, Museum and dangerous stuff always to go last.
    if aName == "Museum" or bName == "Squid" or aData.dangerous then
        a, b = b, a
        aData, bData = bData, aData
        aName, bName = bName, aName
    end

    if aData.collectible and bName == "Museum" then
        -- Collected!
        a:getBody():getUserData().collected = true

        -- Any collectible remaining?
        local anyRemaining = false
        for i, object in ipairs(levelManager.currentLevel().gameLayer.objects) do
            if object.collectible and not object.collected then
                anyRemaining = true
            end
        end
        if not anyRemaining then
            levelManager.currentLevel().won = true
        end
    end

    if (aName == "Squid" or aName == "Jellyfish") and bData.dangerous then
        -- Outch!
        aData:hurt(b:getBody():getUserData())
    end
end

function Level:save(name)
    assert(self.state == "editor")

    local fileName = "levels/" .. name .. ".lua"

    -- Read existing content.
    local file = assert(io.open(fileName, "r"))
    local existingContent = file:read("*all")
    file:close()

    -- Split content.
    local preAndAfterBegin = split(existingContent, "-- begin layers")
    if #preAndAfterBegin ~= 2 then
        print(#preAndAfterBegin)
        error("begin layers comment not found in "..name)
    end
    local preAndAfterEnd = split(preAndAfterBegin[2], "-- end layers")
    if #preAndAfterEnd ~= 2 then
        error("end layers comment not found in "..name)
    end
    local preLayers = preAndAfterBegin[1]
    local afterLayers = preAndAfterEnd[2]

    -- Generate level data.
    contents = ""
    contents = contents .. "layers = {" .. "\n"
    for i,layer in ipairs(self.layers) do
        contents = contents .. "    {" .. "\n"
        contents = contents .. "        polygons = {" .. "\n"
        for i,polygon in ipairs(layer.polygons) do
            contents = contents .. "            {" .. "\n"
            contents = contents .. "                vertices = {" .. "\n"
            for i,vertex in ipairs(polygon.vertices) do
                xStr = tostring(math.floor(vertex.x))
                yStr = tostring(math.floor(vertex.y))
                contents = contents .. "                    vector.new(" .. xStr .. ", " .. yStr .. ")," .. "\n"
            end
            contents = contents .. "                }" .. "\n"
            contents = contents .. "            }," .. "\n"
        end
        contents = contents .. "        }," .. "\n"
        -- TODO use layer.objects
        contents = contents .. "        objects = {" .. "\n"
        for i,object in ipairs(layer.objects) do
            contents = contents .. "                    " .. object:getConstructionString() .. "," .. "\n"
        end
        contents = contents .. "        }" .. "\n"
        contents = contents .. "    }," .. "\n"
    end
    contents = contents .. "}" .. "\n"

    -- Save!
    local file = io.open(fileName, "w")
    io.output(file)
    io.write(preLayers)
    io.write("-- begin layers\n")
    io.write(contents)
    io.write("-- end layers")
    io.write(afterLayers)
    io.close(file)
end


-- go to next level if there is one, return false if not:
function nextLevel()
    assert(levelManager.currentLevel().state == "game")
    if levelManager.current < levelManager.level_count then
        -- got to next level
        levelManager.current = levelManager.current + 1
        levelManager.currentLevel():setState("game")

        -- reset previous level to not won (and not started)
        levels[levelManager.current - 1]:setState("dead")

        return true
    else
        return false
    end
end

-- initialize levels list:
levels = {}

-- this must be called once after the levels have been loaded from files:
function initLevelManager()
    levelManager = {
        current = 1,
        level_count = table.getn(levels),
        currentLevel = function ()
            return levels[levelManager.current]
        end,
    }
end

return Level
