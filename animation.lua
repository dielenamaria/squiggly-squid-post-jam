-- Animation class:

local Animation = class("Animation")

function Animation:initialize(object, duration, frames)
    self.frame = 0
    self.timer = 0.0
    self.frames = frames
    self.frameCount = table.getn(self.frames)
    self.duration = duration
    self.object = object
    self.pauseFrame = false
end

function Animation:update(dt)
    if self.frame == 0 then
        -- do nothing
    elseif self.frame == self.pauseFrame then
        self.object.image = self.frames[self.pauseFrame] --set image
    elseif self.timer < self.duration then
        self.object.image = self.frames[self.frame] --update image
        self.timer = self.timer + dt -- advance timer
        -- set new frame number according to timer:
        self.frame = math.floor(self.timer/(self.duration/self.frameCount)) + 1
    elseif self.timer >= self.duration then
        -- reset timer and stop animation
        self.frame = 0
        self.timer = 0.0
    end
end

function Animation:start()
    self.frame = 1
end

function Animation:pauseOnFrame(frame)
    self.pauseFrame = frame
end

function Animation:resume()
    self.pauseFrame = false
end

return Animation
