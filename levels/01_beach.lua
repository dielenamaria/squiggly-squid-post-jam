local name = "Beach"
local music = music.easylemon

local intro = [[Welcome to your first day of work at the human objects research society, young squid! 
Our team of explorers is excited to accept your help in retrieving interesting objects for our collection.


We spotted three mysterious golden artifacts nearby! Bring all of them to the red seashell museum!


(Press ]]..Input:getKeyString("swim").." to swim forward and "..Input:getKeyString("left").." / "..Input:getKeyString("right")..[[ to turn.
Press ]]..Input:getKeyString("grab")..[[ to grab an object with your arms.)

Press ]]..Input:getKeyString("click").." to start playing."
local outro = "Squid scientists don't know yet what these objects are. \n When humans look at them, they always seem to be unhappy."

-- begin layers
layers = {
    {
        polygons = {
        },
        objects = {
        }
    },
    {
        polygons = {
        },
        objects = {
                    Plants2:new(vector.new(6669, 2661), 0.17184875450422, 8.2944),
                    Plants2:new(vector.new(8180, 2596), -0.56914652388608, 5.76),
                    Plants3:new(vector.new(11472, 3342), 0.0076801603868655, 4),
                    Plants3:new(vector.new(10964, 3256), -0.24461836043328, 6.912),
                    Plants3:new(vector.new(11899, 3193), -1.0206335964502, 8.2944),
        }
    },
    {
        polygons = {
        },
        objects = {
                    Plants2:new(vector.new(2445, 1015), 0.45950930739827, 8.2944),
                    Plants2:new(vector.new(1918, 574), 0.69289104448388, 6.912),
                    Plants2:new(vector.new(6830, 2729), 0.036476927179749, 6.912),
                    Plants3:new(vector.new(10553, 3145), 0.018572838585264, 5.76),
                    Plants3:new(vector.new(11124, 3138), -0.67029636686023, 5.76),
                    Fish14:new(vector.new(10630, 2884), 0, 1),
                    Fish14:new(vector.new(10946, 3009), 0, 1),
                    Fish14:new(vector.new(11001, 2883), 0, 1),
                    Fish14:new(vector.new(10495, 2974), 0, 1),
                    Fish12:new(vector.new(11258, 1449), 0, 1),
                    Fish12:new(vector.new(11444, 1543), 0, 1),
                    Fish12:new(vector.new(11636, 1406), 0, 1),
                    Jellyfish:new(vector.new(10406, 2451), 0, 2.4790145028055),
                    Jellyfish:new(vector.new(10510, 2122), 0, 1.3684851574844),
                    Jellyfish:new(vector.new(10330, 2041), 0, 2.4482207349486),
                    Fish14:new(vector.new(10520, 3083), 0, 1),
                    Fish14:new(vector.new(10794, 3104), 0, 1),
                    Fish14:new(vector.new(10733, 2947), 0, 1),
        }
    },
    {
        polygons = {
        },
        objects = {
                    Plants2:new(vector.new(5655, 2245), 0.47701280729437, 4.8),
                    Plants2:new(vector.new(6171, 2545), 0.13651966744159, 4),
                    Plants2:new(vector.new(6942, 2419), -0.59383118228563, 4.8),
                    Plants3:new(vector.new(9325, 2730), 0.70184420616563, 4),
                    Plants3:new(vector.new(9437, 2773), 0.48480699123801, 4),
                    Plants3:new(vector.new(9438, 2589), 0.045190115011454, 4),
                    Plants3:new(vector.new(10242, 3051), 0.055810347443689, 4),
                    Plants3:new(vector.new(10512, 3036), -0.041575097037118, 4),
                    Plants3:new(vector.new(10324, 2869), -0.2257103307238, 4),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(1988, 1055),
                    vector.new(2244, 1166),
                    vector.new(2680, 1280),
                    vector.new(3049, 1626),
                    vector.new(3391, 1765),
                    vector.new(3775, 1792),
                    vector.new(4059, 1679),
                    vector.new(4359, 1459),
                    vector.new(4672, 1475),
                    vector.new(4976, 1622),
                    vector.new(5581, 2295),
                    vector.new(5812, 2362),
                    vector.new(6139, 2373),
                    vector.new(6688, 2331),
                    vector.new(7633, 1934),
                    vector.new(7892, 1959),
                    vector.new(8149, 2051),
                    vector.new(9312, 2756),
                    vector.new(9713, 2964),
                    vector.new(10151, 3007),
                    vector.new(10720, 2814),
                    vector.new(10866, 2441),
                    vector.new(10773, 2192),
                    vector.new(10641, 2033),
                    vector.new(10491, 1962),
                    vector.new(10375, 1992),
                    vector.new(10279, 2105),
                    vector.new(10273, 2276),
                    vector.new(10333, 2321),
                    vector.new(10449, 2245),
                    vector.new(10571, 2260),
                    vector.new(10614, 2362),
                    vector.new(10567, 2448),
                    vector.new(10423, 2514),
                    vector.new(10325, 2511),
                    vector.new(10147, 2358),
                    vector.new(10083, 2190),
                    vector.new(10118, 2025),
                    vector.new(10297, 1844),
                    vector.new(10506, 1779),
                    vector.new(10802, 1773),
                    vector.new(11696, 1883),
                    vector.new(12115, 1652),
                    vector.new(12243, 1181),
                    vector.new(12409, 467),
                    vector.new(12481, -98),
                    vector.new(12744, -456),
                    vector.new(13228, -498),
                    vector.new(13531, 109),
                    vector.new(13126, 3102),
                    vector.new(12620, 4097),
                    vector.new(10722, 4300),
                    vector.new(8445, 3982),
                    vector.new(6414, 3385),
                    vector.new(5487, 3956),
                    vector.new(4215, 3910),
                    vector.new(2396, 3682),
                    vector.new(1442, 2618),
                    vector.new(-127, 2139),
                    vector.new(-360, 1023),
                    vector.new(-804, -483),
                    vector.new(-421, -618),
                    vector.new(499, -503),
                    vector.new(1189, -29),
                }
            },
            {
                vertices = {
                    vector.new(5865, 1909),
                    vector.new(5663, 1598),
                    vector.new(5843, 1532),
                    vector.new(6058, 1564),
                    vector.new(6298, 1564),
                    vector.new(6585, 1533),
                    vector.new(6771, 1601),
                    vector.new(6606, 1877),
                    vector.new(6226, 1965),
                }
            },
            {
                vertices = {
                    vector.new(7418, 1620),
                    vector.new(7366, 1455),
                    vector.new(7562, 1368),
                    vector.new(7760, 1424),
                    vector.new(7749, 1589),
                    vector.new(7596, 1702),
                }
            },
            {
                vertices = {
                    vector.new(8054, 1125),
                    vector.new(7973, 967),
                    vector.new(8159, 885),
                    vector.new(8348, 946),
                    vector.new(8326, 1092),
                    vector.new(8178, 1194),
                }
            },
            {
                vertices = {
                    vector.new(8235, 1859),
                    vector.new(8203, 1713),
                    vector.new(8346, 1650),
                    vector.new(8536, 1697),
                    vector.new(8482, 1848),
                    vector.new(8340, 1934),
                }
            },
            {
                vertices = {
                    vector.new(8606, 1473),
                    vector.new(8542, 1232),
                    vector.new(8714, 1209),
                    vector.new(8915, 1246),
                    vector.new(9132, 1200),
                    vector.new(9289, 1211),
                    vector.new(9296, 1400),
                    vector.new(9205, 1515),
                    vector.new(9007, 1628),
                    vector.new(8787, 1586),
                }
            },
            {
                vertices = {
                    vector.new(9462, 643),
                    vector.new(9401, 470),
                    vector.new(9381, 298),
                    vector.new(9600, 173),
                    vector.new(10073, 33),
                    vector.new(10139, -364),
                    vector.new(10652, -363),
                    vector.new(10814, 65),
                    vector.new(11371, 237),
                    vector.new(11370, 521),
                    vector.new(11285, 859),
                    vector.new(11094, 1139),
                    vector.new(10844, 997),
                    vector.new(10667, 1285),
                    vector.new(10502, 1060),
                    vector.new(10277, 1442),
                    vector.new(10078, 993),
                    vector.new(9886, 1217),
                    vector.new(9659, 949),
                }
            },
        },
        objects = {
                    Squid:new(vector.new(3578, 1662), 0, 1),
                    Fish1:new(vector.new(3314, 1387), 0, 1),
                    Fish1:new(vector.new(3521, 1281), 0, 1),
                    Fish2:new(vector.new(3264, 1184), 0, 1),
                    Fish3:new(vector.new(3519, 1063), 0, 1),
                    Fish1:new(vector.new(4067, 1442), 0, 1),
                    Fish4:new(vector.new(3764, 1424), 0, 1),
                    Fish2:new(vector.new(2758, 1094), 0, 1),
                    Museum:new(vector.new(6180, 1470), 0, 1),
                    Watch:new(vector.new(8920, 1160), 0, 1),
                    Watch:new(vector.new(6236, 2251), 0, 1),
                    Jellyfish:new(vector.new(11692, 863), 0, 0.55052494346666),
                    Jellyfish:new(vector.new(11890, 601), 0, 0.55162968151971),
                    Jellyfish:new(vector.new(11626, 155), 0, 0.96361335371197),
                    Jellyfish:new(vector.new(12119, 418), 0, 0.81742850362508),
                    Jellyfish:new(vector.new(12074, 1135), 0, 0.91128608037575),
                    Jellyfish:new(vector.new(11552, 1553), 0, 0.81302160884434),
                    Jellyfish:new(vector.new(11440, 1001), 0, 0.99183704609982),
                    Jellyfish:new(vector.new(11779, 1293), 0, 1.1580428732517),
                    Jellyfish:new(vector.new(9473, 2521), 0, 0.84856983238843),
                    Jellyfish:new(vector.new(9680, 2371), 0, 0.54181574332232),
                    Jellyfish:new(vector.new(9456, 2018), 0, 1.1778435318223),
                    Jellyfish:new(vector.new(9939, 1906), 0, 0.95305063336224),
                    Jellyfish:new(vector.new(8966, 2163), 0, 0.92688366651124),
                    Fish2:new(vector.new(8466, 981), 0, 1),
                    Fish2:new(vector.new(8683, 1119), 0, 1),
                    Fish2:new(vector.new(8358, 1227), 0, 0.83333333333333),
                    Fish2:new(vector.new(7880, 1550), 0, 0.69444444444444),
                    Fish2:new(vector.new(8037, 1461), 0, 1.2),
                    Fish2:new(vector.new(8018, 1699), 0, 1),
                    Fish2:new(vector.new(8778, 1894), 0, 1),
                    Fish2:new(vector.new(7770, 1230), 0, 1),
                    Fish1:new(vector.new(6546, 660), 0, 1),
                    Fish1:new(vector.new(6699, 769), 0, 0.83333333333333),
                    Fish1:new(vector.new(6755, 404), 0, 1),
                    Fish1:new(vector.new(6364, 423), 0, 0.69444444444444),
                    Fish14:new(vector.new(9794, 2801), 0, 1),
                    Fish14:new(vector.new(10035, 2667), 0, 1),
                    Fish14:new(vector.new(10328, 2844), 0, 1),
                    Watch:new(vector.new(10340, 2255), 0, 1),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(7843, 1775),
                    vector.new(7746, 2288),
                    vector.new(7439, 2080),
                    vector.new(7528, 1727),
                    vector.new(7275, 1604),
                    vector.new(7336, 1354),
                    vector.new(7785, 1374),
                    vector.new(8283, 1630),
                    vector.new(8170, 1882),
                }
            },
        },
        objects = {
                    Plants2:new(vector.new(2148, 1066), 0.088527491462198, 4),
                    Plants2:new(vector.new(2617, 1171), 0.57704953770799, 4.8),
                    HangingPlant1:new(vector.new(10662, 1312), 0, 4),
                    HangingPlant1:new(vector.new(10824, 1112), -0.24, 4),
                    HangingPlant1:new(vector.new(10564, 1107), 0, 4),
                    HangingPlant1:new(vector.new(10971, 961), -0.96, 4),
                    HangingPlant1:new(vector.new(10091, 1099), 0, 5.76),
                    HangingPlant1:new(vector.new(9566, 1001), 0.48, 5.76),
                    Plants3:new(vector.new(10569, 2586), -0.55030790868863, 4),
                    Plants1:new(vector.new(7476, 1857), -0.29641942852039, 2.3148148148148),
                    Plants1:new(vector.new(7698, 1913), 0.19508297408973, 2.7777777777778),
                    Plants1:new(vector.new(8193, 2086), 0.65574471419229, 2.7777777777778),
                    Plants1:new(vector.new(8478, 2265), 0.34570189109893, 4),
                    Plants2:new(vector.new(8579, 2156), 0.33445503186463, 5.76),
                    Fish14:new(vector.new(9467, 2736), 0, 1),
                    Fish14:new(vector.new(9696, 2760), 0, 1),
                    Fish14:new(vector.new(9707, 2442), 0, 1),
                    Fish14:new(vector.new(9358, 2537), 0, 1),
                    Fish14:new(vector.new(10205, 2701), 0, 1),
                    Fish14:new(vector.new(10312, 2554), 0, 1),
                    Fish14:new(vector.new(10430, 2674), 0, 1),
                    Fish14:new(vector.new(10123, 2537), 0, 1),
                    Fish14:new(vector.new(10511, 2491), 0, 1),
                    Fish14:new(vector.new(9940, 2486), 0, 1),
                    Fish14:new(vector.new(9921, 2868), 0, 1),
                    Fish14:new(vector.new(9729, 2745), 0, 1),
                    Fish14:new(vector.new(9495, 2491), 0, 1),
                    Fish14:new(vector.new(9253, 2728), 0, 1),
                    Jellyfish:new(vector.new(9360, 2059), 0, 0.76744909702414),
                    Jellyfish:new(vector.new(9564, 2321), 0, 0.81477601461356),
                    Jellyfish:new(vector.new(9372, 1722), 0, 0.88364231278743),
                    Jellyfish:new(vector.new(9524, 1587), 0, 0.979489458902),
                    Jellyfish:new(vector.new(9504, 2131), 0, 0.92847718528015),
                    Jellyfish:new(vector.new(9424, 2515), 0, 0.75085368074676),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(7093, 758),
                    vector.new(7572, 736),
                    vector.new(8032, 1108),
                    vector.new(7776, 1330),
                    vector.new(7403, 1121),
                    vector.new(7060, 972),
                }
            },
            {
                vertices = {
                    vector.new(8021, 1175),
                    vector.new(8107, 1792),
                    vector.new(7997, 2584),
                    vector.new(7416, 2358),
                    vector.new(7594, 2108),
                    vector.new(7700, 1521),
                    vector.new(7700, 1134),
                }
            },
        },
        objects = {
                    Plants2:new(vector.new(2026, 1039), -0.12884556651099, 4),
                    Plants2:new(vector.new(1627, 757), 0.48712625304569, 4),
                    Jellyfish:new(vector.new(8654, 1855), 0, 0.44745058537298),
                    Jellyfish:new(vector.new(8482, 1797), 0, 0.36250662275355),
                    Jellyfish:new(vector.new(8602, 1972), 0, 0.37487894007307),
                    Jellyfish:new(vector.new(8567, 1376), 0, 0.40987465283889),
                    Jellyfish:new(vector.new(8520, 1658), 0, 0.44054006379914),
                    Jellyfish:new(vector.new(8558, 1184), 0, 0.39450733901498),
                    Jellyfish:new(vector.new(8478, 1721), 0, 0.37554246182559),
                    Jellyfish:new(vector.new(8552, 1568), 0, 0.64068543622333),
                    Jellyfish:new(vector.new(8514, 1858), 0, 0.38017456903),
                    Jellyfish:new(vector.new(8483, 1943), 0, 0.30162267041423),
                    Jellyfish:new(vector.new(8486, 1731), 0, 0.5237505242967),
                    Jellyfish:new(vector.new(8447, 1468), 0, 0.40648653433598),
                    Jellyfish:new(vector.new(8636, 1575), 0, 0.35909654592137),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(6268, 1157),
                    vector.new(6259, 563),
                    vector.new(6076, 256),
                    vector.new(6289, 138),
                    vector.new(6529, 325),
                    vector.new(6576, 1025),
                    vector.new(6526, 2202),
                    vector.new(6162, 2041),
                }
            },
        },
        objects = {
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(1689, 401),
                    vector.new(1715, 168),
                    vector.new(1828, 93),
                    vector.new(1950, 412),
                    vector.new(2388, 1834),
                    vector.new(1587, 1703),
                    vector.new(1655, 595),
                }
            },
        },
        objects = {
                    Plants1:new(vector.new(2037, 612), 1.356028783472, 4),
                    Plants1:new(vector.new(2051, 1002), 0.80724830506023, 6.912),
                    Plants1:new(vector.new(2259, 1153), 0.5648091519563, 4),
        }
    },
}
-- end layers

level = Level:new(name, layers, intro, outro, music, 8000)

return level
