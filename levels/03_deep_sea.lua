local name = "Deep Sea"
local music = music.arcadia
local intro = "By now, you're an experienced explorer! We've gotten word of three very strange human objects in this part of the ocean. Nosquid has been able to retrieve them yet! Good luck!"
local outro = "Well done! We believe that the spiky object is for taming lightning! The heavy one might be a house for small humans? And the one made from glass... contains a message! Ready to hear it?"

-- begin layers
layers = {
    {
        polygons = {
        },
        objects = {
                    Plants2:new(vector.new(2667, 3063), 1.6091633763854, 11.943936),
                    Plants2:new(vector.new(3047, 4704), -1.4306658940172, 8.2944),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(1017, 74),
                    vector.new(1510, 275),
                    vector.new(1681, 536),
                    vector.new(2113, 695),
                    vector.new(1983, 950),
                    vector.new(1167, 1320),
                    vector.new(813, 1147),
                    vector.new(805, 473),
                }
            },
        },
        objects = {
                    Plants2:new(vector.new(3591, 1305), -0.25700270148081, 8.2944),
                    Plants2:new(vector.new(2886, 2410), -1.4740547312902, 8.2944),
                    Plants2:new(vector.new(1733, 3895), 1.5044298553003, 9.95328),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(1863, 793),
                    vector.new(1302, 847),
                    vector.new(936, 413),
                    vector.new(1459, 75),
                }
            },
        },
        objects = {
                    Plants2:new(vector.new(5785, 1233), 0.52561346414131, 5.76),
                    Plants2:new(vector.new(2953, 1304), -0.35925500247128, 4),
                    Plants2:new(vector.new(3454, 1189), -0.51211089450103, 5.76),
                    Corals4:new(vector.new(4339, 3709), 0.64, 3.456),
                    Corals4:new(vector.new(5086, 3983), -0.24, 4.1472),
                    Corals4:new(vector.new(4768, 4241), 0, 4.97664),
                    Corals4:new(vector.new(3956, 4025), 0.6, 7.1663616),
                    Boat:new(vector.new(1945, 420), 0.48, 4.97664),
        }
    },
    {
        polygons = {
        },
        objects = {
                    Plants2:new(vector.new(6221, 1285), -0.12947660601788, 4.8),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(894, -274),
                    vector.new(1600, -73),
                    vector.new(1717, 231),
                    vector.new(1822, 374),
                    vector.new(2134, 432),
                    vector.new(2558, 268),
                    vector.new(2787, 129),
                    vector.new(3086, 218),
                    vector.new(3226, 394),
                    vector.new(3284, 609),
                    vector.new(3130, 783),
                    vector.new(2943, 864),
                    vector.new(2545, 863),
                    vector.new(2339, 862),
                    vector.new(2153, 902),
                    vector.new(2052, 1090),
                    vector.new(2055, 1305),
                    vector.new(2080, 1684),
                    vector.new(2073, 1843),
                    vector.new(2083, 1948),
                    vector.new(2390, 1961),
                    vector.new(2403, 2153),
                    vector.new(2403, 2408),
                    vector.new(1955, 2398),
                    vector.new(1813, 2377),
                    vector.new(1747, 2401),
                    vector.new(1746, 2823),
                    vector.new(1400, 3137),
                    vector.new(1308, 3503),
                    vector.new(1314, 3780),
                    vector.new(1377, 3953),
                    vector.new(1488, 4115),
                    vector.new(1746, 4196),
                    vector.new(1751, 4325),
                    vector.new(1746, 4454),
                    vector.new(1409, 4824),
                    vector.new(1355, 5003),
                    vector.new(1381, 5182),
                    vector.new(1496, 5316),
                    vector.new(1708, 5396),
                    vector.new(1916, 5392),
                    vector.new(2092, 5342),
                    vector.new(2267, 5084),
                    vector.new(2492, 5330),
                    vector.new(2235, 5642),
                    vector.new(1759, 5640),
                    vector.new(1190, 5544),
                    vector.new(824, 5081),
                    vector.new(1102, 4150),
                    vector.new(640, 3840),
                    vector.new(260, 2679),
                    vector.new(758, 2129),
                    vector.new(648, 1547),
                    vector.new(766, 872),
                }
            },
            {
                vertices = {
                    vector.new(3858, 688),
                    vector.new(3872, 991),
                    vector.new(3880, 1387),
                    vector.new(4001, 1741),
                    vector.new(4262, 1885),
                    vector.new(4381, 1946),
                    vector.new(4310, 2125),
                    vector.new(4167, 2166),
                    vector.new(3689, 2305),
                    vector.new(3652, 2567),
                    vector.new(3759, 2755),
                    vector.new(4381, 2940),
                    vector.new(4324, 3165),
                    vector.new(4086, 3347),
                    vector.new(4158, 3545),
                    vector.new(4490, 3588),
                    vector.new(4847, 3562),
                    vector.new(5073, 3498),
                    vector.new(5464, 3276),
                    vector.new(5596, 2975),
                    vector.new(5259, 2708),
                    vector.new(4992, 2671),
                    vector.new(4737, 2577),
                    vector.new(4696, 2417),
                    vector.new(5253, 2293),
                    vector.new(5655, 2207),
                    vector.new(5735, 2127),
                    vector.new(6091, 2133),
                    vector.new(6456, 2164),
                    vector.new(6409, 2431),
                    vector.new(5793, 2774),
                    vector.new(5796, 3241),
                    vector.new(5770, 3677),
                    vector.new(5406, 3965),
                    vector.new(4228, 3919),
                    vector.new(3087, 3805),
                    vector.new(2268, 5073),
                    vector.new(2186, 5005),
                    vector.new(2082, 4763),
                    vector.new(1820, 4458),
                    vector.new(1817, 4318),
                    vector.new(1819, 4200),
                    vector.new(2138, 4021),
                    vector.new(2393, 3808),
                    vector.new(2448, 3470),
                    vector.new(2403, 3194),
                    vector.new(2169, 2973),
                    vector.new(1959, 2890),
                    vector.new(1897, 2811),
                    vector.new(1896, 2659),
                    vector.new(1898, 2529),
                    vector.new(2539, 2536),
                    vector.new(2556, 2428),
                    vector.new(2538, 2287),
                    vector.new(2533, 2047),
                    vector.new(2522, 1837),
                    vector.new(2215, 1812),
                    vector.new(2230, 1524),
                    vector.new(2232, 1301),
                    vector.new(3191, 1091),
                    vector.new(3610, 660),
                }
            },
            {
                vertices = {
                    vector.new(4244, 672),
                    vector.new(4538, 653),
                    vector.new(4805, 1017),
                    vector.new(5522, 1163),
                    vector.new(5828, 1219),
                    vector.new(6271, 1142),
                    vector.new(6563, 825),
                    vector.new(6692, 305),
                    vector.new(6879, 299),
                    vector.new(6915, 924),
                    vector.new(6884, 1302),
                    vector.new(6869, 1742),
                    vector.new(6852, 2071),
                    vector.new(6867, 2482),
                    vector.new(6892, 3083),
                    vector.new(7162, 3405),
                    vector.new(7354, 3757),
                    vector.new(7683, 3973),
                    vector.new(7881, 4030),
                    vector.new(8369, 3991),
                    vector.new(8976, 3682),
                    vector.new(9081, 4088),
                    vector.new(9181, 5050),
                    vector.new(9328, 5828),
                    vector.new(9354, 6005),
                    vector.new(9273, 6108),
                    vector.new(9005, 5901),
                    vector.new(8547, 5857),
                    vector.new(8214, 6039),
                    vector.new(8110, 6434),
                    vector.new(8342, 6760),
                    vector.new(9130, 6794),
                    vector.new(9414, 6682),
                    vector.new(9740, 6167),
                    vector.new(10533, 6122),
                    vector.new(10627, 6595),
                    vector.new(10504, 7164),
                    vector.new(9306, 7225),
                    vector.new(8761, 7453),
                    vector.new(8175, 7306),
                    vector.new(7829, 7020),
                    vector.new(7534, 6218),
                    vector.new(7444, 5299),
                    vector.new(7194, 4918),
                    vector.new(7310, 4687),
                    vector.new(6899, 4249),
                    vector.new(5847, 4362),
                    vector.new(5889, 3428),
                    vector.new(5830, 2779),
                    vector.new(6467, 2439),
                    vector.new(6452, 2155),
                    vector.new(5750, 2105),
                    vector.new(5627, 1899),
                    vector.new(5169, 1699),
                    vector.new(4655, 1597),
                    vector.new(4472, 1429),
                    vector.new(4229, 1184),
                }
            },
            {
                vertices = {
                    vector.new(5095, 312),
                    vector.new(5491, -149),
                    vector.new(5974, -675),
                    vector.new(6690, -625),
                    vector.new(8011, -470),
                    vector.new(8584, 999),
                    vector.new(9933, 2174),
                    vector.new(10274, 2450),
                    vector.new(10697, 2722),
                    vector.new(11044, 4074),
                    vector.new(10888, 4996),
                    vector.new(10741, 5619),
                    vector.new(10410, 6014),
                    vector.new(9703, 6098),
                    vector.new(9669, 5705),
                    vector.new(9570, 4979),
                    vector.new(9561, 4127),
                    vector.new(9490, 3370),
                    vector.new(9398, 3174),
                    vector.new(9111, 2930),
                    vector.new(8452, 2952),
                    vector.new(8056, 2915),
                    vector.new(7670, 3037),
                    vector.new(7470, 3163),
                    vector.new(7261, 3039),
                    vector.new(7274, 1922),
                    vector.new(7263, 1342),
                    vector.new(7246, 936),
                    vector.new(7096, -110),
                    vector.new(6779, -218),
                    vector.new(6486, -97),
                    vector.new(6250, 787),
                    vector.new(6103, 997),
                    vector.new(5717, 980),
                    vector.new(5274, 874),
                    vector.new(5034, 758),
                    vector.new(4821, 559),
                }
            },
            {
                vertices = {
                    vector.new(1604, 3317),
                    vector.new(1508, 3299),
                    vector.new(1534, 3221),
                    vector.new(1612, 3158),
                    vector.new(1673, 3216),
                    vector.new(1688, 3290),
                }
            },
            {
                vertices = {
                    vector.new(1782, 3104),
                    vector.new(1745, 3025),
                    vector.new(1868, 2983),
                    vector.new(1986, 2996),
                    vector.new(1957, 3115),
                    vector.new(1895, 3171),
                }
            },
            {
                vertices = {
                    vector.new(2024, 3260),
                    vector.new(2110, 3155),
                    vector.new(2225, 3197),
                    vector.new(2253, 3307),
                    vector.new(2138, 3407),
                    vector.new(2001, 3389),
                }
            },
            {
                vertices = {
                    vector.new(1703, 3433),
                    vector.new(1812, 3364),
                    vector.new(1875, 3435),
                    vector.new(1887, 3530),
                    vector.new(1785, 3578),
                    vector.new(1677, 3523),
                }
            },
            {
                vertices = {
                    vector.new(2072, 3692),
                    vector.new(2024, 3581),
                    vector.new(2126, 3541),
                    vector.new(2211, 3559),
                    vector.new(2226, 3642),
                    vector.new(2162, 3729),
                }
            },
            {
                vertices = {
                    vector.new(1840, 3885),
                    vector.new(1642, 3913),
                    vector.new(1493, 3786),
                    vector.new(1468, 3628),
                    vector.new(1647, 3615),
                    vector.new(1832, 3658),
                    vector.new(1920, 3751),
                }
            },
            {
                vertices = {
                    vector.new(4008, 438),
                    vector.new(3955, 321),
                    vector.new(4096, 335),
                }
            },
            {
                vertices = {
                    vector.new(4726, 3148),
                    vector.new(4683, 2924),
                    vector.new(5114, 2881),
                    vector.new(5332, 3065),
                    vector.new(5118, 3279),
                    vector.new(4809, 3338),
                }
            },
            {
                vertices = {
                    vector.new(4038, 2646),
                    vector.new(3863, 2524),
                    vector.new(4060, 2364),
                    vector.new(4376, 2419),
                    vector.new(4322, 2609),
                }
            },
            {
                vertices = {
                    vector.new(4718, 2105),
                    vector.new(4683, 1945),
                    vector.new(5169, 1894),
                    vector.new(5370, 2013),
                    vector.new(5153, 2180),
                    vector.new(4811, 2217),
                }
            },
            {
                vertices = {
                    vector.new(7798, 3228),
                    vector.new(8121, 3163),
                    vector.new(8501, 3244),
                    vector.new(8647, 3482),
                    vector.new(8439, 3713),
                    vector.new(8069, 3834),
                    vector.new(7772, 3784),
                    vector.new(7570, 3495),
                }
            },
            {
                vertices = {
                    vector.new(4219, 344),
                    vector.new(4359, 310),
                    vector.new(4298, 450),
                }
            },
        },
        objects = {
                    Squid:new(vector.new(2025, 148), 0, 1),
                    Fork:new(vector.new(1797, 5350), 0, 1),
                    Museum:new(vector.new(4147, 229), 0, 1),
                    Suitcase:new(vector.new(4521, 3482), 0, 1),
                    Seaurchin:new(vector.new(4421, 3075), 0, 1),
                    Seaurchin:new(vector.new(4638, 3073), 0, 1),
                    Seaurchin:new(vector.new(4419, 2517), 0, 1),
                    Seaurchin:new(vector.new(4641, 2512), 0, 1),
                    Seaurchin:new(vector.new(4410, 2052), 0, 1),
                    Seaurchin:new(vector.new(4641, 2059), 0, 1),
                    Seaurchin:new(vector.new(6787, 238), 0, 1),
                    Bottle:new(vector.new(8693, 5929), 0, 1),
                    Seaurchin:new(vector.new(7609, 3231), 0, 1),
                    Jellyfish:new(vector.new(1806, 84), 0, 0.76594698223622),
                    Jellyfish:new(vector.new(1836, 285), 0, 0.57207098688715),
                    Jellyfish:new(vector.new(2267, 286), 0, 0.36859204350959),
                    Jellyfish:new(vector.new(2210, 83), 0, 0.78004143222098),
                    Jellyfish:new(vector.new(1988, 341), 0, 0.78606257853316),
                    Jellyfish:new(vector.new(1571, 4914), 0, 0.57904718716768),
                    Jellyfish:new(vector.new(1812, 4821), 0, 0.91917901634327),
                    Jellyfish:new(vector.new(2023, 5079), 0, 0.56955833767674),
                    Jellyfish:new(vector.new(1616, 5202), 0, 0.22050794831986),
                    Fish1:new(vector.new(5033, 185), 0, 1),
                    Fish1:new(vector.new(4932, 304), 0, 0.69444444444444),
                    Fish1:new(vector.new(4820, 125), 0, 0.69444444444444),
                    FireJelly:new(vector.new(8730, 3049), 0, 0.99108923283706),
                    FireJelly:new(vector.new(8947, 3318), 0, 0.62305058100112),
                    FireJelly:new(vector.new(8927, 3059), 0, 0.44288864639884),
                    Plants2:new(vector.new(5126, 1049), 0.19692659204774, 4),
                    Plants2:new(vector.new(6111, 1267), -0.67755644614899, 4.8),
                    Plants2:new(vector.new(6399, 1087), -0.91082851148142, 4),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(4218, 270),
                    vector.new(4234, 438),
                    vector.new(3891, 485),
                    vector.new(3867, 317),
                }
            },
        },
        objects = {
                    HangingPlant1:new(vector.new(1649, 3861), 0, 2.3148148148148),
                    HangingPlant1:new(vector.new(2080, 3635), 0, 1.1163265889346),
                    HangingPlant1:new(vector.new(2099, 3312), -0.2, 1.6075102880658),
                    HangingPlant1:new(vector.new(1561, 3243), 0, 1.1163265889346),
                    HangingPlant2:new(vector.new(1828, 3081), 0, 1.3395919067215),
                    HangingPlant2:new(vector.new(1699, 3481), 0.28, 1.3395919067215),
                    HangingPlant2:new(vector.new(1770, 3484), -0.36, 1.3395919067215),
                    Corals1:new(vector.new(2109, 946), 2.48, 2),
                    Corals1:new(vector.new(2219, 881), 2.76, 1.6666666666667),
                    Corals1:new(vector.new(2034, 1033), 2.08, 1.1574074074074),
                    Corals2:new(vector.new(2474, 893), 3.12, 1.6666666666667),
                    Corals1:new(vector.new(3899, 1585), 1, 2),
                    Corals1:new(vector.new(4039, 1740), 0.28, 1.6666666666667),
                    Corals1:new(vector.new(3808, 1330), 1.32, 2),
                    Corals4:new(vector.new(4029, 3356), 0.6, 1.3888888888889),
                    Corals4:new(vector.new(4678, 3428), -0.28, 1.1574074074074),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(3814, 888),
                    vector.new(3685, 514),
                    vector.new(3445, 462),
                    vector.new(3301, 608),
                    vector.new(3176, 940),
                    vector.new(3057, 779),
                    vector.new(3287, 349),
                    vector.new(3564, 277),
                    vector.new(3788, 383),
                    vector.new(4029, 779),
                }
            },
        },
        objects = {
                    Fish3:new(vector.new(3779, 2432), 0, 0.48225308641975),
                    Fish3:new(vector.new(3898, 2272), 0, 0.33489797668038),
                    Fish3:new(vector.new(3712, 1968), 0, 0.40187757201646),
                    Fish3:new(vector.new(4177, 1996), 0, 0.33489797668038),
                    Fish3:new(vector.new(4229, 1707), 0, 0.5787037037037),
                    Fish3:new(vector.new(4208, 2398), 0, 0.5787037037037),
                    Fish3:new(vector.new(3946, 2217), 0, 0.5787037037037),
                    Fish3:new(vector.new(3418, 2386), 0, 0.5787037037037),
                    Fish3:new(vector.new(3184, 2173), 0, 0.40187757201646),
                    Fish3:new(vector.new(4988, 1749), 0, 0.5787037037037),
                    Corals4:new(vector.new(4272, 3056), 0, 0.55816329446731),
                    Corals4:new(vector.new(4205, 3140), -0.76, 0.55816329446731),
                    Corals4:new(vector.new(4289, 3121), 0.84, 0.46513607872276),
                    Corals4:new(vector.new(3662, 3127), 0, 0.66979595336077),
                    Corals4:new(vector.new(3627, 3005), 1.44, 0.96450617283951),
        }
    },
    {
        polygons = {
            {
                vertices = {
                    vector.new(4147, 3690),
                    vector.new(2807, 3667),
                    vector.new(2423, 3049),
                    vector.new(2088, 1659),
                    vector.new(2682, 1323),
                    vector.new(2938, 1974),
                    vector.new(3103, 2185),
                    vector.new(3189, 3033),
                    vector.new(3544, 3151),
                    vector.new(3858, 2888),
                    vector.new(4111, 2551),
                    vector.new(4239, 1898),
                    vector.new(4188, 1631),
                    vector.new(4408, 1039),
                    vector.new(5099, 1085),
                    vector.new(4864, 1642),
                    vector.new(5085, 3097),
                }
            },
        },
        objects = {
        }
    },
    {
        polygons = {
        },
        objects = {
        }
    },
}
-- end layers

level = Level:new(name, layers, intro, outro, music, 24000)

return level
