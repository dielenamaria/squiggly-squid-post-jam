require "lib.slam"
require "lib.helpers"
vector = require "lib.hump.vector"
Camera = require "lib.hump.camera"
tlfres = require "lib.tlfres"
class = require 'lib.middleclass' -- see https://github.com/kikito/middleclass
libroomy = require 'lib.roomy' -- see https://github.com/tesselode/roomy
Input = require "lib.input.Input"
Cursor = require "cursor"

Animation = require "animation"

Gradient = require "gradient"

Object = require "objects.object"
PhysicsObject = require "objects.physicsobject"
Squid = require "objects.squid"
Jellyfish = require "objects.jellyfish"
Fish = require "objects.fish"
Seaurchin = require "objects.seaurchin"
Plant = require "objects.plant"
Museum = require "objects.museum"
require "objects.artifacts"
require "objects.deco"
require "objects.animals"

Level = require "level"
Layer = require "layer"
inspect = require('lib.inspect')
debugWorldDraw = require('lib.debugWorldDraw')

CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080
roomy = libroomy.new() -- roomy is the manager for all the rooms (scenes) in our game
scenes = {} -- initialize list of scenes

textColor = {0.6,0.7,1.0,1.0}
secondaryColor = {0.6,0.6,0.8,1.0}
muted = false

function love.load()
    -- initialize randomness in two ways:
    love.math.setRandomSeed(os.time())
    math.randomseed(os.time())

    -- set up default drawing options
    love.graphics.setBackgroundColor(0.1, 0.1, 0.1)

    -- load assets
    images = {}
    for file in listUsefulFiles("images") do
        images[file.basename] = love.graphics.newImage(file.path)
    end

    sounds = {}
    for file in listUsefulFiles("sounds") do
        sounds[file.basename] = love.audio.newSource(file.path, "static")
        sounds[file.basename]:setVolume(0.4)
    end
    sounds.grab:setVolume(0.1)

    music = {}
    for file in listUsefulFiles("music") do
        music[file.basename] = love.audio.newSource(file.path, "stream")
        music[file.basename]:setLooping(true)
        music[file.basename]:setVolume(0.5)
    end
    music.waterlily:setVolume(0.9)
    music.underwater:setVolume(0.3)

    fonts = {}
    for file in listUsefulFiles("fonts") do
        fonts[file.basename] = {}
        sizes = {30,50,200}
        for i, fontsize in ipairs(sizes) do
            fonts[file.basename][fontsize] = love.graphics.newFont(file.path, fontsize)
        end
    end

    --love.graphics.setNewFont(40) -- initialize default font size
    love.graphics.setFont(fonts.CaveatBold[50])

    -- levels
    basenames = {}
    for file in listUsefulFiles("levels") do
        table.insert(basenames, file.basename)
    end
    table.sort(basenames) --sort level filenames in alphabetical order
    for i, level in ipairs(basenames) do
        levels[i] = require ("levels/" .. level)
        levels[i].filename = level
    end

    -- scenes
    for file in listUsefulFiles("scenes") do
        scenes[file.basename] = require ("scenes." .. file.basename)
    end

    initLevelManager() -- this must be done after the levels are loaded from the filesystem

    roomy:hook({exclude = {"draw", "update"}}) --hook roomy in to manage the scenes (with exceptions)
    roomy:enter(scenes.title) --start on title screen

    input = Input:new()

    love.graphics.setLineJoin("bevel")

    print("\n\n\nEdit '" .. configFilePath .. "' to configure the input mapping.\n\n\n")

end

function love.update(dt)
    input:update(dt)
    handleInput()
    roomy:emit("handleInput")
    roomy:emit("update", math.min(dt, 0.05))
end

function love.mousemoved(x, y, dx, dy, istouch)
    input:mouseMoved(dx, dy)
end

-- not sure if we need this, only makes sense with absolute mouse positions
function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

function handleInput()
    if input:isPressed("quit") then
        love.window.setFullscreen(false)
        love.event.quit()
    end
    if input:isPressed("fullscreen") then
        isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen)
    end
    if input:isPressed("mute") then
        muted = not muted
        l = levelManager.currentLevel()
        if l then
            if muted then
                l.music:stop()
            else
                l.music:play()
            end
        end
    end
end

function love.draw()
    love.graphics.setColor(1, 1, 1) -- white
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

    -- (draw any global stuff here)
    -- Show this somewhere to the user so they know where to configure
    --love.graphics.printf("Edit '" .. configFilePath .. "' to configure the input mapping.", 0, 990, CANVAS_WIDTH, "center")

    -- draw scene-specific stuff:
    roomy:emit("draw")

    tlfres.endRendering()
end
