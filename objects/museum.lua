local Museum = class("Museum", PhysicsObject)

function Museum:initialize(position, angle, scale)
    PhysicsObject.initialize(self, position.x, position.y, 300, 200, images.snail_shell3)
    self.drawScale = 0.5
    self.gravity = 500000
    self.gravityAir = 10000000
end

return Museum
