Watch = class("Watch", PhysicsObject)
function Watch:initialize(position, angle, scale)
    PhysicsObject.initialize(self, position.x, position.y, 80, 80, images.pocket_watch)
    self.drawScale = 0.4
    self.collectible = true
    self.gravity = 5000
    self.gravityAir = 7000
end

Bottle = class("Bottle", PhysicsObject)
function Bottle:initialize(position, angle, scale)
    PhysicsObject.initialize(self, position.x, position.y, 80, 80, images.bottle)
    self.drawScale = 0.4
    self.collectible = true
    self.gravity = -40000
    self.gravityAir = 50000
end

Fork = class("Fork", PhysicsObject)
function Fork:initialize(position, angle, scale)
    PhysicsObject.initialize(self, position.x, position.y, 280, 20, images.fork1)
    self.drawScale = 0.4
    self.collectible = true
    self.gravity = 2000
    self.gravityAir = 10000
end

Suitcase = class("Suitcase", PhysicsObject)
function Suitcase:initialize(position, angle, scale)
    PhysicsObject.initialize(self, position.x, position.y, 200, 120, images.suitcase)
    self.drawScale = 0.4
    self.collectible = true
    self.gravity = 40000
    self.gravityAir = 1000000
end
