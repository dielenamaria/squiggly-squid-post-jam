HangingPlant1= class("HangingPlant1", Object)
function HangingPlant1:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.sihouette_hanging_plants1)
    self.angle = angle or 0
    self.scale = scale or 4
end

HangingPlant2 = class("HangingPlant2", Object)
function HangingPlant2:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.sihouette_hanging_plants2)
    self.angle = angle or 0
    self.scale = scale or 4
end

Plants1 = class("Plants1", Plant)
function Plants1:initialize(position, angle, scale)
    Plant.initialize(self, position, angle, scale, images.sihouette_plants1, 1, 2)
end

Plants2 = class("Plants2", Plant)
function Plants2:initialize(position, angle, scale)
    Plant.initialize(self, position, angle, scale, images.sihouette_plants2, 1, 2)
end

Plants3 = class("Plants3", Plant)
function Plants3:initialize(position, angle, scale)
    Plant.initialize(self, position, angle, scale, images.sihouette_plants3, 1, 2)
end


Corals1 = class("Corals1", Object)
function Corals1:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.sihouette_corals1)
    self.angle = angle or 0
    self.scale = scale or 2
end


Corals2 = class("Corals2", Object)
function Corals2:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.sihouette_corals2)
    self.angle = angle or 0
    self.scale = scale or 2
end


Corals3 = class("Corals3", Object)
function Corals3:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.sihouette_corals3)
    self.angle = angle or 0
    self.scale = scale or 2
end

Corals4 = class("Corals4", Object)
function Corals4:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.coral2)
    self.angle = angle or 0
    self.scale = scale or 2
end

Corals5 = class("Corals5", Object)
function Corals5:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.coral3)
    self.angle = angle or 0
    self.scale = scale or 2
end

Corals6 = class("Corals6", Object)
function Corals6:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.coral1)
    self.angle = angle or 0
    self.scale = scale or 2
end

Kelp1 = class("Kelp1", Object)
function Kelp1:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.kelp1)
    self.angle = angle or 0
    self.scale = scale or 2
end

Kelp2 = class("Kelp2", Object)
function Kelp2:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.kelp2)
    self.angle = angle or 0
    self.scale = scale or 2
end

Boat = class("Boat", Object)
function Boat:initialize(position, angle, scale)
    Object.initialize(self, position.x, position.y, 50, 30, images.shipwreck1)
    self.angle = angle or 0
    self.scale = scale or 2
end