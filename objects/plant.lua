local Plant = class("Plant", Object)
function Plant:initialize(position, angle, scale, image, speedMin, speedMax)
    Object.initialize(self, position.x, position.y, 50, 30, image)
    self.angle = angle or 0
    self.scale = scale or 4
    self.speed = math.random(speedMin or 0.1, speedMax or 0.2)
    self.phase = math.random(10)
    self.wiggle = 0.1
end

function Plant:update(dt)
    self.phase = self.phase + self.speed * dt
end

function Plant:getAngle()
    return self.angle + math.sin(self.phase) * self.wiggle
end

return Plant