Fish1 = class ("Fish1", Fish)
function Fish1:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish1
    self.drawScale = 0.25
    self.color = { r=1, g=1, b=1}
end

Fish2 = class ("Fish2", Fish)
function Fish2:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish2
    self.drawScale = 0.3
    self.color = { r=1, g=1, b=1}
end

Fish3 = class ("Fish3", Fish)
function Fish3:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish3
    self.drawScale = 0.2
    self.color = { r=1, g=1, b=1}
end

Fish4 = class ("Fish4", Fish)
function Fish4:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish4
    self.drawScale = 0.3
    self.color = { r=1, g=1, b=1}
end

Fish5 = class ("Fish5", Fish)
function Fish5:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish5
    self.drawScale = 0.1
    self.color = { r=1, g=1, b=1}
end

Fish6 = class ("Fish6", Fish)
function Fish6:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish6
    self.drawScale = 0.1
    self.color = { r=1, g=1, b=1}
end

Fish7 = class ("Fish7", Fish)
function Fish7:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish7
    self.drawScale = 0.1
    self.color = { r=1, g=1, b=1}
end

Fish8 = class ("Fish8", Fish)
function Fish8:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish8
    self.drawScale = 0.1
    self.color = { r=1, g=1, b=1}
end

Fish9 = class ("Fish9", Fish)
function Fish9:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish9
    self.drawScale = 0.1
    self.color = { r=1, g=1, b=1}
end

Fish11 = class ("Fish11", Fish)
function Fish11:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish11
    self.drawScale = 0.1
    self.color = { r=1, g=1, b=1}
end

Fish12 = class ("Fish12", Fish)
function Fish12:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish12
    self.drawScale = 0.1
    self.color = { r=0.3, g=0.2, b=0.5}
end

Fish13 = class ("Fish13", Fish)
function Fish13:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish13
    self.drawScale = 0.1
    self.color = { r=1, g=1, b=1}
end

Fish14 = class ("Fish14", Fish)
function Fish14:initialize(position, angle, scale)
    Fish.initialize(self, position, angle, scale)
    self.image = images.fish14
    self.drawScale = 0.1
    self.color = { r=1, g=1, b=1}
end

FireJelly = class ("FireJelly", Jellyfish)
function FireJelly:initialize(position, angle, scale)
    Jellyfish.initialize(self, position, angle, scale)
    self.color = {
        r = 0.8+0.2*love.math.random(),
        g = 0.4+0.2*love.math.random(),
        b = 0.2+0.2*love.math.random(),
        a = 0.7+0.2*love.math.random(),
    }
    self.dangerous = true
end