local PhysicsObject = class("PhysicsObject", Object)

function PhysicsObject:initialize(x, y, width, height, image) 
    assert(image ~= nil, "Must image!")
    Object.initialize(self, x, y, width, height, image)
end

function PhysicsObject:materialize(world)
    local pos = self:getPosition()
    self.body = love.physics.newBody(world, pos.x, pos.y, "dynamic")
    self.body:setUserData(self)

    local width = self:getWidth()
    local height = self:getHeight()

    -- Build a shape that fits into this rectangle.
    if width == height then
        local circleShape = love.physics.newCircleShape(0, 0, width/2)
        love.physics.newFixture(self.body, circleShape)
    elseif width > height then
        local rectangleShape = love.physics.newRectangleShape(0, 0, width-height, height)
        local circleShape1 = love.physics.newCircleShape(-(width-height)/2, 0, height/2)
        local circleShape2 = love.physics.newCircleShape((width-height)/2, 0, height/2)
        love.physics.newFixture(self.body, rectangleShape)
        love.physics.newFixture(self.body, circleShape1)
        love.physics.newFixture(self.body, circleShape2)
    else
        local rectangleShape = love.physics.newRectangleShape(0, 0, width, height-width)
        local circleShape1 = love.physics.newCircleShape(0, -(height-width)/2, width/2)
        local circleShape2 = love.physics.newCircleShape(0, (height-width)/2, width/2)
        love.physics.newFixture(self.body, rectangleShape)
        love.physics.newFixture(self.body, circleShape1)
        love.physics.newFixture(self.body, circleShape2)
    end

    self.angularDamping = 5
    self.linearDamping = 2
    self.materialized = true
end

function PhysicsObject:dematerialize(world)
    self.body:destroy()
    self.body = nil
    self.materialized = false
end

function PhysicsObject:update(dt)
    if self:getPosition().y > 0 then -- under water
        local velX, velY = self.body:getLinearVelocity()
        local vel = vector(velX, velY)
        local misalign = 0
        if self.class.name == "Squid" then
            local dirX, dirY = self.body:getWorldVector(0, 1)
            local dir = vector(dirX, dirY)
            misalign = math.abs(dir:cross(vel:normalized()))
    
            --print ("Squid damping: " .. self.linearDamping .. " linear * " .. (1 + 2 * misalign * misalign) .. " misalign " .. " + " .. (vel:len() / 3000) .. " velocity")
        end

    

        self.body:setAngularDamping(self.angularDamping)
        self.body:setLinearDamping (self.linearDamping * (1 + 2 * misalign * misalign) + vel:len() / 3000)
    else -- above
        self.body:setAngularDamping(self.angularDamping / 10)
        self.body:setLinearDamping (self.linearDamping / 10)
    end
end

function PhysicsObject:getPosition()
    if self.materialized then
        local x, y = self.body:getPosition()
        return vector(x, y)
    else
        return self.position
    end
end

function PhysicsObject:getAngle()
    if self.materialized then
        return self.body:getAngle()
    else
        return self.angle
    end
end

function PhysicsObject:setPosition(position)
    assert(vector.isvector(position), "Position must be vector")
    if self.materialized then
        self.body:setPosition(position.x, position.y)
    end
    self.position = position -- TODO maybe don't change this
end

function PhysicsObject:setAngle(angle)
    if self.materialized then
        self.body:setAngle(angle)
    end
    self.angle = angle -- TODO maybe don't change this
end

return PhysicsObject
