local Fish = class("Fish", PhysicsObject)

function Fish:initialize(position, angle, scale)
    PhysicsObject.initialize(self, position.x, position.y, 50, 30, images.fish1)
    self.healed = false
    self.color = {
        r = 0.5+0.5*love.math.random(),
        g = 0.5+0.5*love.math.random(),
        b = 0.5+0.5*love.math.random(),
    }
    self.angle = angle or 0
    self.scale = scale or 1

    self.gravity = math.random(-200, 200);
    self.gravityAir = 20000;
end

function Fish:update(dt)
    PhysicsObject.update(self, dt)
    -- swim
    local randomSwim = math.random(1,10000*dt)

    if randomSwim < 2 then
        self.mirrored = not self.mirrored
    end
    if randomSwim < 8 then
        -- swimming physics:
        local angle = self.body:getAngle() -- math.pi/2
        if self.mirrored then
            angle = angle +  math.pi
        end
        local forwardFactor = 10*self.scale*self.scale
        -- draw back a bit to original position
        -- here we benefit from self:getPosition() != self.position
        dif = self:getPosition() - self.position
        self.body:applyLinearImpulse(math.cos(angle)*forwardFactor - dif.x * 0.003, math.sin(angle)*forwardFactor - dif.y  * 0.003)
    end



    -- rotating upwards:
    local angle = self.body:getAngle()
    angle = angle % (2*math.pi)
    local dir = 0
    if angle < math.pi then
        dir = -1
    else
        dir = 1
    end
    self.body:applyTorque(dt*10000*self.scale*dir)
end

function Fish:draw()
    love.graphics.setColor(self.color.r, self.color.g, self.color.b)
    Object.draw(self)
    love.graphics.setColor(1, 1, 1)
end

return Fish
