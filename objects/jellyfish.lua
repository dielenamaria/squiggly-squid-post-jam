local Jellyfish = class("Jellyfish", PhysicsObject)

function Jellyfish:initialize(position, angle, scale)
    self.base_image = images.jellyfish5

    PhysicsObject.initialize(self, position.x, position.y, 50, 50, self.base_image)
    self.angle = angle or 0
    self.scale = scale or 0.75+0.25*love.math.random()

    self.gravity = 400;
    self.gravityAir = 90000;

    self.color = {
        r = 0.5+0.3*love.math.random(),
        g = 0.6+0.3*love.math.random(),
        b = 0.8+0.2*love.math.random(),
        a = 0.5+0.2*love.math.random(),
    }

    -- stuff for swimming animation
    local swimAnimationFrames = {
        images.jellyfish5,
        images.jellyfish4,
        images.jellyfish3,
        images.jellyfish2,
        images.jellyfish1,
        images.jellyfish1,
        images.jellyfish2,
        images.jellyfish3,
        images.jellyfish4,
        images.jellyfish5,
        images.jellyfish6,
        images.jellyfish6,
        images.jellyfish5,
    }
    self.swimAnimation = Animation:new(self, 1.5, swimAnimationFrames)

    self.world = world
end

function Jellyfish:materialize(world)
    local pos = self:getPosition()
    self.body = love.physics.newBody(world, pos.x, pos.y, "dynamic")
    self.body:setUserData(self)
    local circleShape1 = love.physics.newCircleShape(0, -self:getHeight()/2, self:getWidth()/2)
    love.physics.newFixture(self.body, circleShape1)
    self.angularDamping = 5
    self.linearDamping = 6
    self.materialized = true
end

function Jellyfish:update(dt)
    PhysicsObject.update(self, dt)

    local randomSwim = math.random(1,200)

    if randomSwim == 1 then
        -- start swim animation:
        self.swimAnimation:start()
        -- swimming physics:
        local angle = self.body:getAngle() - math.pi/2
        local forwardFactor = 30*self.scale*self.scale
        self.body:applyLinearImpulse(math.cos(angle)*forwardFactor, math.sin(angle)*forwardFactor)
    end

    -- drifting forwards/backwards:
    local velocity = vector(self.body:getLinearVelocity())
    local angle = vector(0, -1):rotated(self.body:getAngle())
    local drift = velocity:projectOn(angle)*dt*30
    self.body:applyForce(drift.x, drift.y)

    -- rotating upwards:
    local angle = self.body:getAngle()
    angle = angle % (2*math.pi)
    local dir = 0
    if angle < math.pi then
        dir = -1
    else
        dir = 1
    end
    self.body:applyTorque(dt*10000*self.scale*dir)

    self.swimAnimation:update(dt)

end

function Jellyfish:draw()
    love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.a)

    local angle = self:getAngle()
    local pos = self:getPosition()
    local extraSquish = 1
    if self.materialized then
        local vx, vy = self.body:getLinearVelocity()
        local speed = math.sqrt(vx*vx + vy*vy)
        local extraSquish = clamp(1, speed/400, 1.1)
    end
    local scale = self:getScale()*self.drawScale
    if self.image then
        love.graphics.draw(self.image, pos.x, pos.y, angle, scale, scale*extraSquish, self.image:getWidth()/2, self.image:getHeight()/2)
    end

    love.graphics.setColor(1, 1, 1, 1)
end

function Jellyfish:hurt(byWhat)
    Object.hurt(self, byWhat, 100)
end

return Jellyfish
