local Object = class("Object")

function Object:initialize(x, y, width, height, image)
    assert(image ~= nil, "Must image!")
    self.image = image
    self.position = vector(x, y)
    self.width = width
    self.height = height
    self.angle = 0
    self.scale = 1
    self.drawScale = 0.2
end

function Object:getPosition()
    return self.position
end

function Object:getAngle()
    return self.angle
end

function Object:getScale()
    return self.scale
end

function Object:getWidth()
    return self.width * self.scale
end

function Object:getHeight()
    return self.height * self.scale
end

function Object:setPosition(position)
    assert(vector.isvector(position), "Position must be vector")
    self.position = position
end

function Object:setAngle(angle)
    self.angle = angle
end

function Object:setScale(scale)
    self.scale = scale
end

function Object:draw()
    local angle = self:getAngle()
    local pos = self:getPosition()
    local scale = self:getScale()*self.drawScale
    if not self.image then
        love.graphics.printf(self.class.name, pos.x, pos.y, 200, "center")
    else
        m = 1
        if self.mirrored then
            m = -1
        end
        love.graphics.draw(self.image, pos.x, pos.y, angle, scale * m, scale, self.image:getWidth()/2, self.image:getHeight()/2)
    end
end

function Object:materialize(world)
    -- do nothing here, but each object should have this method
end

function Object:dematerialize(world)
    -- do nothing here, but each object should have this method
end

function Object:hurt(byWhat, amount)
    local direction = (vector(self.body:getPosition()) - vector(byWhat.body:getPosition())):normalized()*amount
    self.body:applyLinearImpulse(direction.x, direction.y)
end

function Object:getConstructionString()
    xStr = tostring(math.floor(self:getPosition().x))
    yStr = tostring(math.floor(self:getPosition().y))
    angleStr = tostring(self:getAngle()):gsub(",",".")
    scaleStr = tostring(self:getScale()):gsub(",",".")
    return self.class.name .. ":new(vector.new(" .. xStr .. ", " .. yStr .. "), " .. angleStr .. ", " .. scaleStr .. ")"
end

return Object
