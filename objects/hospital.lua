local Hospital = class("Hospital", PhysicsObject)

function Hospital:initialize(world)
    PhysicsObject.initialize(self, 250, 1, 350, 250, images.hospital)
    self:setPosition(vector(love.math.random(CANVAS_WIDTH), love.math.random(CANVAS_HEIGHT)))
    --self.body:getFixtures()[1]:setSensor(true)
end

return Hospital
