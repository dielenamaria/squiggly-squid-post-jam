local scene = {} 

function scene:draw()
    love.graphics.setColor(secondaryColor)
    love.graphics.setFont(fonts.CaveatBold[50])
    love.graphics.printf([[It reads: Thanks for playing! <3]], 0, CANVAS_HEIGHT/2-70, CANVAS_WIDTH, "center")
    love.graphics.setColor(1,1,1,1)
end

return scene
