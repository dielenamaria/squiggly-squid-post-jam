local scene = {}
local Camera = require("lib.hump.camera")

local types = {
    Museum,
    Watch,
    Bottle,
    Fork,
    Suitcase,
    Jellyfish,
    FireJelly,
    Seaurchin,
    Plants1,
    Plants2,
    Plants3,
    HangingPlant1,
    HangingPlant2,
    Corals1,
    Corals2,
    Corals3,
    Corals4,
    Corals5,
    Corals6,
    Kelp1,
    Kelp2,
    Stones1,
    Stones2,
    Fish1,
    Fish2,
    Fish3,
    Fish4,
    Fish5,
    Fish6,
    Fish7,
    Fish8,
    Fish9,
    Fish11,
    Fish12,
    Fish13,
    Fish14,
    Boat,
}

function scene:enter()
    levelManager:currentLevel():setState("editor")

    self.music = music.ambient_starfield
    --self.music:play()
    self.cursor = Cursor:new("geometry", "free", CANVAS_WIDTH / 2, CANVAS_HEIGHT / 2)
    circle = {} --initialize geometry object with draw function
    function circle:draw(x, y)
        love.graphics.setColor(1,0,0)
        love.graphics.circle("line", x, y, 20)
        love.graphics.setColor(1,1,1)
    end
    self.cursor:setGeometry(circle)

    love.mouse.setRelativeMode(true)
    self.camera = Camera()
    self.smoother = Camera.smooth.damped(1)

    self.selectedPolygon = nil
    self.selectedVertex = nil
    self.selectedObject = nil

    self.level = levelManager.currentLevel()
    self.level.debug = false
    self.onlyLayer = false
    self.activeLayer = 5

    self.typeIndex = 1
end

function scene:update(dt)
    self.cursor:move()
    border = 150 
    self.camera:lockWindow(self.cursor.pos_x, self.cursor.pos_y, border, CANVAS_WIDTH - border, border, CANVAS_HEIGHT - border) --, self.smoother)

    if self.selectedVertex then
        self.selectedVertex.x = math.floor(self.cursor.pos_x)
        self.selectedVertex.y = math.floor(self.cursor.pos_y)
    end
    if self.selectedObject then
        self.selectedObject:setPosition(self.cursor:getPos())
    end
end

function scene:draw()
    aL = self.level.layers[self.activeLayer]

    love.graphics.setColor(1, 1, 1) --white

    if self.onlyLayer then
        self.level:draw(self.camera, self.activeLayer)
    else
        self.level:draw(self.camera)
    end
    
    self.camera:attach(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT, true, self.level.layers[self.activeLayer].z)
    nVertex, nPolygon = aL:getNearestVertex(self.cursor:getPos(), 50)
    nSegPolygon, nSegVextexIndex = aL:getNearestSegment(self.cursor:getPos(), 50)
    if nVertex == nil then
        nObject = aL:getNearestObject(self.cursor:getPos(), 150)
    end
    
    -- draw outlines and circles
    for i,polygon in ipairs(aL.polygons) do
        count = #polygon.vertices
        for i=1,count do
            v1 = polygon.vertices[(i - 1) % count + 1]
            v2 = polygon.vertices[(i - 0) % count + 1]
            love.graphics.setColor(0.8,0.8,0.8)
            if nSegPolygon == polygon and i % count + 1 == nSegVextexIndex then
                love.graphics.setColor(0.4,0.4,1.0)
            end
            love.graphics.line(v1.x, v1.y, v2.x, v2.y)

            if nVertex == v1 then
                love.graphics.setColor(0.4,0.4,1.0)
                love.graphics.circle("fill", v1.x, v1.y, 5)
            else
                love.graphics.circle("line", v1.x, v1.y, 5)
            end
        end
    end

    if self.selectedPolygon then
        love.graphics.setColor(0.8,0.4,0.4)
        count = #self.selectedPolygon.vertices
        for i=1,count do
            v1 = self.selectedPolygon.vertices[(i - 1) % count + 1]
            v2 = self.selectedPolygon.vertices[(i - 0) % count + 1]
            if v1 == self.selectedVertex or v2 == self.selectedVertex then
                love.graphics.line(v1.x, v1.y, v2.x, v2.y)
            end
        end
    end

    if nObject then
        love.graphics.setColor(0.8,0.8,0.8)
        love.graphics.circle("line", nObject:getPosition().x, nObject:getPosition().y, 100)
    end

    self.cursor:draw()
    self.camera:detach()

    love.graphics.printf(self:getTyp().name, CANVAS_WIDTH / 2, CANVAS_HEIGHT - 160, 1300, "center")
end

function scene:getTyp()
    return indexedWithWrap(types, self.typeIndex)
end

function scene:handleInput()
    aL = self.level.layers[self.activeLayer]
    for i=1,9 do
        if input:isPressed("layer" .. i) then
            self.activeLayer = i
        end
    end

    if input:isPressed("onlyLayer") then
        self.onlyLayer = not self.onlyLayer
    end

    if input:isDown("rotateLeft") and self.selectedObject then
        self.selectedObject.angle = self.selectedObject.angle - 0.04
    end
    if input:isDown("rotateRight") and self.selectedObject then
        self.selectedObject.angle = self.selectedObject.angle + 0.04
    end

    if input:isPressed("click") then
        if self.selectedVertex then
            aL:preparePolygons(self.level.gradient)
            self.selectedVertex = nil
            self.selectedPolygon = nil
        else
            if self.selectedObject then
                self.selectedObject = nil
            else
                self.selectedVertex, self.selectedPolygon = aL:getNearestVertex(self.cursor:getPos(), 50)
                if self.selectedVertex == nil then
                    self.selectedObject = aL:getNearestObject(self.cursor:getPos(), 150)
                end
            end
        end
    end

    if input:isPressed("insertVertex") then
        if self.selectedVertex == nil then
            self.selectedPolygon, vertexIndex = aL:getNearestSegment(self.cursor:getPos(), 50)
            if self.selectedPolygon then
                self.selectedVertex = self.cursor:getPos():clone()
                table.insert(self.selectedPolygon.vertices, vertexIndex, self.selectedVertex)
            end
        else
            aL:preparePolygons(self.level.gradient)
            self.selectedVertex = nil
            self.selectedPolygon = nil
        end
    end


    if input:isPressed("insertObject") then
        if self.selectedVertex == nil and self.selectedObject == nil then
           object = self:getTyp():new(self.cursor:getPos())
           table.insert(aL.objects, object)
           self.selectedObject = object
        end
    end

    if input:isPressed("nextType") then
        self.typeIndex = self.typeIndex + 1
    end   
    if input:isPressed("previousType") then
        self.typeIndex = self.typeIndex - 1
    end

    if input:isPressed("newPolygon") then
        if self.selectedVertex == nil then
            firstVertex = self.cursor:getPos():clone()
            polygon = { vertices = { firstVertex } }
            table.insert(aL.polygons, polygon)
        end
    end

    if input:isPressed("deleteVertex") then
        if self.selectedObject then
            removeValue(aL.objects, self.selectedObject)
            self.selectedObject = nil
        else
            if self.selectedVertex == nil then
                self.selectedVertex, self.selectedPolygon = aL:getNearestVertex(self.cursor:getPos(), 50)
            end
            if self.selectedVertex ~= nil then
                removeValue(self.selectedPolygon.vertices, self.selectedVertex)
                print("deleted vertex. remaining: " .. #self.selectedPolygon.vertices)
                if #self.selectedPolygon.vertices == 0 then
                    removeValue(aL.polygons, self.selectedPolygon)
                end
                aL:preparePolygons(self.level.gradient)
                self.selectedVertex = nil
                self.selectedPolygon = nil
            end
        end
    end

    if input:isPressed("menu") then
        roomy:push(scenes.menu)
    end

    if input:isPressed("editor") then
        roomy:enter(scenes.gameplay)
    end

    if input:isPressed("save") then
        self.level:save(self.level.filename)
    end

    if input:isPressed("zoomin") then
        if self.selectedObject then
            self.selectedObject.scale = self.selectedObject.scale * 1.2
        else
            self.camera:zoom(1.5)
        end
    end

    if input:isPressed("zoomout") then
        if self.selectedObject then
            self.selectedObject.scale = self.selectedObject.scale / 1.2
        else
            self.camera:zoom(0.5)
        end
    end
end

return scene
