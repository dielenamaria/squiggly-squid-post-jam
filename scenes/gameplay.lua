local scene = {}

function scene:enter()
    initLevelManager() -- this must be done after the levels are loaded from the filesystem
    levelManager:currentLevel():setState("game")

    self.camera = Camera(0, 0, 1)

    self.atmo = music.underwater
    self.atmo:play()
    self.cameraTargets = {}
    love.mouse.setRelativeMode(true)
end

function scene:pause()
    levelManager:currentLevel().musicPosition = levelManager:currentLevel().music:tell()
    levelManager:currentLevel().music:pause()
    self.atmo:stop()
end

function scene:leave()
    self.atmo:stop()
end

function scene:resume()
    self.atmo:play()

    levelManager:currentLevel().music:stop()
    if not muted then
        levelManager:currentLevel().music:play()
        pos = levelManager:currentLevel().musicPosition
        if pos then
            levelManager:currentLevel().music:seek(pos)
        end
    end
end

function scene:update(dt)
    levelManager.currentLevel():update(dt)

    -- Camera position:
    local velocity = vector(levelManager.currentLevel().squid.body:getLinearVelocity())
    local targetLookAt = velocity*2.3
   
    -- Keep the latest 20 targetLookAt-values, and compute an average of them.
    -- The current position will be added after the average is computed.
    -- Otherwise, "outdated current" positions would be part of the average.
    table.insert(self.cameraTargets, targetLookAt)
    while #self.cameraTargets > 20 do
        table.remove(self.cameraTargets, 1)
    end
    local avgTarget = vector(0, 0)
    for i,target in ipairs(self.cameraTargets) do
        avgTarget = avgTarget + target
    end
    avgTarget = avgTarget / #self.cameraTargets + vector(levelManager.currentLevel().squid.body:getPosition())
    
    local actualLookAt = lerp(vector(self.camera:position()), avgTarget, 0.5*dt)
    self.camera:lookAt(actualLookAt.x, actualLookAt.y)
    -- Camera zoom:
    local speed = velocity:len()
    local targetZoom = 0.6*1.8/(0.75+range(speed, 300, 1000))
    local actualZoom = lerp(self.camera.scale, targetZoom, 0.5*dt)
    self.camera:zoomTo(actualZoom)
    -- Sounds:
    music.underwater:setVolume(range(speed, -10, 1000))

    -- Hack camera reset!
    if levelManager.currentLevel().wantsCameraReset then
        local squidPos = levelManager:currentLevel().squid:getPosition()
        self.cameraTargets = {}
        self.camera:lookAt(squidPos.x, squidPos.y)
        self.camera:zoomTo(2)
        levelManager.currentLevel().wantsCameraReset = false
    end
end

function scene:draw()
    love.graphics.setColor(1, 1, 1) --white

    local level = levelManager.currentLevel()

    level:draw(self.camera)
end

function scene:handleInput()
    if input:isPressed("click") then
        if levelManager.currentLevel().started == false then
            -- start level from intro
            levelManager.currentLevel():start()
        elseif levelManager.currentLevel():isWon() then
            if nextLevel() == false then    -- goes to next level if there is one
                roomy:enter(scenes.credits) -- show credits screen
            end
        else
            -- gameplay stuff
        end
    end

    if input:isPressed("pause") then
        roomy:push(scenes.pause)
    end

    if input:isPressed("menu") then
        roomy:push(scenes.menu)
    end

    -- cheat to win current level
    if input:isPressed("cheat") then
        levelManager.currentLevel().won = true
    end

    if input:isPressed("editor") then
        roomy:enter(scenes.editor)
    end
end

return scene
