local Layer = class("Layer")

local zValues = {
    -75,
    -30,
    -15,
    -5,
    0, -- actual gameplay
    5,
    25,
    50,
    100
}

function Layer:initialize(index)
    self.index = index
    self.z = zValues[index]
    self.objects = {}
    self.polygons = {}
    self.blackPolygons = {}
    self.edgeMeshes = {}

    self.hasEdges = (index == 5)
end

function Layer:cleanupWorld()
    for i, object in ipairs(self.objects) do
        object:dematerialize()
    end
    self.world:destroy()
    self.world = nil
end

function Layer:cleanupPolygons()
    self.blackPolygons = {}
    self.edgeMeshes = {}
end

function Layer:update(dt)
    for i, object in ipairs(self.objects) do
        if object.update ~= nil then
            object:update(dt)
        end
        if object.gravity then
            local gravity = object.gravity
            if object:getPosition().y < 0 and object.gravityAir then
                gravity = object.gravityAir
            end
            object.body:applyForce(0, gravity*object.scale*object.scale*dt)
        end
    end
    self.world:update(dt)
end

function Layer:draw(camera, debug, world)
    camera:attach(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT, true, self.z)
    
    love.graphics.setColor(0.0,0.0,0.0)
    for i,polygon in ipairs(self.blackPolygons) do
        love.graphics.polygon('fill', polygon)
    end

    -- if self.index < 3 then
    --     love.graphics.setColor(0.25,0.25, 0.25)
    -- elseif self.index < 5 then
    --     love.graphics.setColor(0.5,0.5, 0.5)
    -- elseif self.index == 5 then 
    --     love.graphics.setColor(1,1,1)
    -- elseif self.index == 6 then
    --     love.graphics.setColor(0.75,0.75, 1)
    -- elseif self.index == 7 then
    --     love.graphics.setColor(0.55,0.55, 0.8)
    -- elseif self.index == 8 then
    --     love.graphics.setColor(0.35,0.35, 0.75)
    -- else 
    --     love.graphics.setColor(0.25,0.25,0.5)
    -- end

    love.graphics.setColor(1,1,1)

    for i,mesh in ipairs(self.edgeMeshes) do
        love.graphics.draw(mesh)
    end

    -- draw squid last
    squid = nil
    for i, object in ipairs(self.objects) do
        if object.class.name ~= "Squid" then
            object:draw()
        else
            squid = object
        end
    end
    if squid then
        squid:draw()
    end

    if debug then
        local worldX, worldY = camera:worldCoords(0, 0)
        debugWorldDraw(self.world,worldX,worldY,CANVAS_WIDTH, CANVAS_HEIGHT)
    end
    
    -- for x=0,10000,1000 do
    --     for y=0,10000,1000 do
    --         love.graphics.printf("Layer " .. self.index , x, y, CANVAS_WIDTH, "center")
    --     end
    -- end
    camera:detach()
end

function Layer:getNearestObject(pos, maxDistance)
    minDistance = maxDistance * 2
    nearestObject = nil
   
    for i,object in ipairs(self.objects) do
        dist = pos:dist(object:getPosition())
        if dist < minDistance then
            nearestObject = object
            minDistance = dist
        end
    end

    if maxDistance == nil or minDistance < maxDistance then
        return nearestObject
    else
        return nil
    end
end

function Layer:getNearestVertex(pos, maxDistance)
    minDistance = maxDistance * 2
    nearestVextex = nil
    nearestPolygon = nil

    for i,polygon in ipairs(self.polygons) do
        for i,vertex in ipairs(polygon.vertices) do
            dist = pos:dist(vertex)
            if dist < minDistance then
                nearestVextex = vertex
                nearestPolygon = polygon
                minDistance = dist
            end
        end
    end

    if maxDistance == nil or minDistance < maxDistance then
        return nearestVextex, nearestPolygon
    else
        return nil, nil
    end
end

function Layer:getNearestSegment(pos, maxDistance)
    minDistance = maxDistance * 2
    nearestPolygon = nil
    nearestVextexIndex = 0

    for i,polygon in ipairs(self.polygons) do
        count = #polygon.vertices
        for i=1,count do
            i1 = (i - 1) % count + 1
            i2 = (i - 0) % count + 1
            v1 = polygon.vertices[i1]
            v2 = polygon.vertices[i2]
            b = v1 - v2
            segmentLen = b:len()
            
            lineDist = math.abs((pos - v1):cross(b)) / b:len()
            v1Dist = math.max(0, pos:dist(v1) - segmentLen)
            v2Dist = math.max(0, pos:dist(v2) - segmentLen)
            dist = math.max(lineDist, v1Dist, v2Dist)

            if dist < minDistance then
                nearestPolygon = polygon
                nearestVextexIndex = i2
                minDistance = dist
            end
        end
    end

    if maxDistance == nil or minDistance < maxDistance then
        return nearestPolygon, nearestVextexIndex
    else
        return nil, nil
    end
end

function Layer:prepareWorld()
    -- Set up world.
    love.physics.setMeter(100)
    self.world = love.physics.newWorld(0, 0, true)
    self.world:setCallbacks(beginContact)

    for i,object in ipairs(self.objects) do
        object:materialize(self.world)
    end

    for i, polygon in ipairs(self.polygons) do
        for i=1,#polygon.vertices-1 do
            self:createWall(polygon.vertices[i].x, polygon.vertices[i].y, polygon.vertices[i+1].x, polygon.vertices[i+1].y)
        end
        self:createWall(polygon.vertices[#polygon.vertices].x, polygon.vertices[#polygon.vertices].y, polygon.vertices[1].x, polygon.vertices[1].y)
    end
end

-- Adapted from A Bloody Small World :)
function Layer:createWall(x1, y1, x2, y2)
    wall = {}
    wall.body = love.physics.newBody(self.world, 0, 0)
    wall.shape = love.physics.newEdgeShape(x1, y1, x2, y2)
    wall.fixture = love.physics.newFixture(wall.body, wall.shape)
    wall.fixture:setFriction(1)

    --wall.x1 = x1
    --wall.y1 = y1
    --wall.x2 = x2
    --wall.y2 = y2

    --table.insert(walls, wall)
end

function Layer:preparePolygons(gradient)
    self.blackPolygons = {}
    self.edgeMeshes = {}

    local layerFactor = (math.abs(self.index - 5) + 1)
    
    stripes = 24

    -- for each polygon…
    for i,polygon in ipairs(self.polygons) do
        -- computed from the input 
        tangents = {}
        innerVertices = {}
        
        -- output
        blackVertices = {} -- for use as polygon
        edgeVertices = {} -- for use as mesh (triangle strip)

        -- tmp
        roundness = {}
        prevP3i = nil 
        u = 0
        v = 0
        count = #polygon.vertices
        
        -- precompute tangents according to direct neighbours
        for i=1,count do
            v1 = indexedWithWrap(polygon.vertices, i - 1) 
            v2 = indexedWithWrap(polygon.vertices, i)     
            v3 = indexedWithWrap(polygon.vertices, i + 1) 
            tangent = (v3 - v1):normalized()
            table.insert(tangents, tangent)
            r = pseudoRandom(v2.x + v2.y * 1.712, 10, 270)
            table.insert(roundness, r)
            iv = v2 + tangent:perpendicular() * r
            table.insert(innerVertices, iv)
        end

        -- for each segment
        for i=1,count do
            -- get both vertices and their tangents
            v1 = indexedWithWrap(polygon.vertices, i)
            v2 = indexedWithWrap(polygon.vertices, i + 1)
            t1 = indexedWithWrap(tangents,i)
            t2 = indexedWithWrap(tangents,i + 1)
            r1 = indexedWithWrap(roundness,i)
            r2 = indexedWithWrap(roundness,i + 1)
            l = v1:dist(v2) * 0.4

            -- inner vertexes
            v1i = indexedWithWrap(innerVertices, i)
            v2i = indexedWithWrap(innerVertices, i + 1)
            
            steps = 7
            for ai=0,steps-1 do
                a = ai / steps
                b = 1.0-a

                -- interpolate for outer
                p1 = (v1 + t1 * a * l)
                p2 = (v2 - t2 * b * l)
                p3 = p1 * pseudoSin(a) + p2 * pseudoSin(b)

                -- interpolate for inner
                p1i = (v1i + t1 * a * l)
                p2i = (v2i - t2 * b * l)
                p3i = p1i * pseudoSin(a) + p2i * pseudoSin(b)

                -- roundness
                r = r1 * b + r2 * a

                -- add black (polygon) vertices
                table.insert(blackVertices, p3.x)
                table.insert(blackVertices, p3.y)

                if prevP3i then
                    off = p3i:dist(prevP3i) / 1800
                    u = u + off
                    v = v + off / stripes
                end

                -- add edge (mesh) vertices
                -- we just need to add the inner vertex, then the outer vertex
                -- at the end of the polygon loop, we need to add the first two vertices again

                -- vertex attributes:
                -- The table filled with vertex information tables for each vertex as follows:

                -- number [1] -- The position of the vertex on the x-axis.
                -- number [2] -- The position of the vertex on the y-axis.
                -- number [3] (0) -- The u texture coordinate of the vertex. Texture coordinates are normally in the range of [0, 1], but can be greater or less (see WrapMode.)
                -- number [4] (0) -- The v texture coordinate of the vertex. Texture coordinates are normally in the range of [0, 1], but can be greater or less (see WrapMode.)
                -- number [5] (1) -- The red component of the vertex color.
                -- number [6] (1) -- The green component of the vertex color.
                -- number [7] (1) -- The blue component of the vertex color.
                -- number [8] (1) -- The alpha component of the vertex color.
                local cr,cg,cb = gradient:getColorByY(p3.y + 200 * layerFactor)

                evInner = {p3i.x, p3i.y, u, v + (r / 50) / stripes, 0, 0, 0, 1 }
                evOuter = {p3.x , p3.y , u, v + 0, cr, cg, cb, 1 }
                table.insert(edgeVertices, evInner)
                table.insert(edgeVertices, evOuter)

                prevP3i = p3i
            end
        end

        -- postprocess and save vertices for black polygon
        if not love.math.isConvex(blackVertices) then
            -- triangulate may throw an error, and there's no easy way to detected this before it happens
            success, triangles = pcall(love.math.triangulate, blackVertices)
            if success then
                for i,triangle in ipairs(triangles) do
                    table.insert(self.blackPolygons, triangle)
                end
            else
                print("Error during triangulation: " .. triangles)
            end
        else    
            table.insert(self.blackPolygons, blackVertices)
        end

        -- postprocess and save vertices for edge mesh
        -- create duplicates of v1 and v2, but with adjusted uv coordinates
        v1 = edgeVertices[1]
        v2 = edgeVertices[2]
        evInner = {v1[1], v1[2], math.ceil(u), v + 1.0 / stripes, 0, 0, 0, 1 }
        evOuter = {v2[1], v2[2], math.ceil(u), v + 0, v1[5], v1[6], v1[7], 1 }
        table.insert(edgeVertices, evInner)
        table.insert(edgeVertices, evOuter)
        mesh = love.graphics.newMesh(edgeVertices, "strip", "static")
        images.polii_5:setWrap("repeat")
        mesh:setTexture(images.polii_5)
        table.insert(self.edgeMeshes, mesh)
    end
end

return Layer
