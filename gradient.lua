local Gradient = class("Gradient")

function Gradient:initialize(maxDepth)
    self.steps = {
        { y = -2000,                    color={ 17,  70, 160}},
        { y =  -800,                    color={ 78, 140, 190}},
        { y =     0,                    color={170, 200, 220}},
        { y =     1 * maxDepth / 15000, color={ 61, 146, 204}},
        { y =    50 * maxDepth / 15000, color={ 25, 116, 134}},
        { y =  2000 * maxDepth / 15000, color={ 10,  84,  97}},
        { y =  5000 * maxDepth / 15000, color={  0,   0,   0}},
        { y = 15000 * maxDepth / 15000, color={  0,   0,   0}},
    }

    local vertices = {}
    for i,step in ipairs(self.steps) do
        table.insert(vertices,  {             -1000, step.y, 0, 0, step.color[1]/255, step.color[2]/255, step.color[3]/255, 1 })
        table.insert(vertices,  {CANVAS_WIDTH +1000, step.y, 0, 0, step.color[1]/255, step.color[2]/255, step.color[3]/255, 1 })
    end

    self.mesh = love.graphics.newMesh(vertices, "strip", "static")
end

function Gradient:draw(camera)
    camera:attach(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT, true, 0)
    cx, cy = camera:position()
    love.graphics.draw(self.mesh, cx - CANVAS_WIDTH / 2)
    camera:detach()
end

function Gradient:getColorByY(y)
    prevColor = {1,1,1}
    prevY = -3000
    local r,g,b = 1,1,1
    for i,step in ipairs(self.steps) do
        if step.y > y then
            stepColor = step.color

            if y < 500 then
                stepColor = {1,1,1}
            end

            -- step.y is the greater y
            local span = step.y - prevY

            local a = (y - prevY) / span
            r = (stepColor[1] * a + prevColor[1] * (1-a)) / 255
            g = (stepColor[2] * a + prevColor[2] * (1-a)) / 255
            b = (stepColor[3] * a + prevColor[3] * (1-a)) / 255
            break
        end
        prevColor = step.color
        prevY = step.y
    end

    skyFactor = clamp(0, (3800 - y) / 3800, 1)

    r = math.min(1, r * (1-skyFactor) + 1 * skyFactor)
    g = math.min(1, g * (1-skyFactor) + 1 * skyFactor)
    b = math.min(1, b * (1-skyFactor) + 1 * skyFactor)

    return r,g,b
end

return Gradient
